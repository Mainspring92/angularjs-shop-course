'use strict';

var adminCtrls = angular.module('adminCtrls', ['angularFileUpload', 'directives']);

//============ PRODUCTS CONTROLLERS=====================

adminCtrls.controller('products',['$scope', '$filter','$http', function($scope, $filter, $http){

  $http({
    method: 'GET',
    url: 'model/produkty.json'
  }).then(function successCallback( response ){
    $scope.products = response.data;
  },function errorCallback( response ){
    console.log('Blad pobrania pliku');
  });

  $scope.deleteProduct = function( product, $index ){
if (!confirm('Czy chcesz usunac produkt ?'))
return false;
        $scope.products.splice($index,1);
        console.log(product);
  };
}]);

adminCtrls.controller('productEdit',['$routeParams','$scope','$http','FileUploader', function($routeParams, $scope, $http, FileUploader){
  $http({
    method: 'GET',
    url: 'model/produkty.json'
  }).then(function successCallback( response ){
    $scope.products = response.data;
    $scope.product = $scope.products[$routeParams.id];
  },function errorCallback( response ){
    console.log('Blad pobrania pliku');
  });

  $scope.saveChanges = function( product ){
        console.log(product);
    console.log($routeParams.id);

  };

// ============= IMAGE UPLOADS =============

var uploader = $scope.uploader = new FileUploader({
url : '' // API path for drop images
});

// ===================== FILTERS FOR IMAGES ====================

  uploader.filters.push({
      name: 'imageFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
          var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
          return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
  });

  uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };

}]);

adminCtrls.controller('productCreate',['$scope', function($scope){
  $scope.createProduct = function( ){
        console.log($scope.product);
  };

}]);

//============ USERS CONTROLLERS=====================

adminCtrls.controller('users',['$scope', '$http', function($scope, $http){

$http({
  method: 'GET',
  url: 'model/users.json'
}).then(function successCallback( response ){
  $scope.users = response.data;
},function errorCallback( response ){
  console.log('Blad pobrania pliku');
});

$scope.deleteUser = function(user, $index ){

  if (!confirm('Czy chcesz usunac uzytkownika ?'))
  return false;
      $scope.users.splice($index,1);
      console.log(user);
};

}]);

adminCtrls.controller('userEdit',['$routeParams','$scope','$http', function($routeParams, $scope, $http){
  $http({
    method: 'GET',
    url: 'model/users.json'
  }).then(function successCallback( response ){
    $scope.users = response.data;
    $scope.user = $scope.users[$routeParams.id];
  },function errorCallback( response ){
    console.log('Blad pobrania pliku');
  });

  $scope.saveChanges = function( user ){
        console.log(user);
    console.log($routeParams.id);

  };

}]);

adminCtrls.controller('userCreate',['$scope', function($scope){
  $scope.createUser = function( ){
        console.log($scope.user);
  };
}]);

//============ ORDERS CONTROLLERS=====================

adminCtrls.controller('orders',['$scope', '$http', function($scope, $http){

$http({
  method: 'GET',
  url: 'model/orders.json'
}).then(function successCallback( response ){
  $scope.orders = response.data;
},function errorCallback( response ){
  console.log('Blad pobrania pliku');
});

$scope.deleteOrder = function(order, $index ){
  if (!confirm('Czy chcesz usunac zamowienie ?'))
  return false;
      $scope.orders.splice($index,1);
      console.log(order);
};


$scope.changeStatus = function (order){


   if(order.status == 0)
   {
      order.status = 1;
   }
   else if (order.status == 1) {
     order.status = 2;
   }
  else
  order.status = 0;
    console.log(order.status);
};


  $scope.orderDetails = function($index) {
    var orderButton = angular.element( document.querySelector( '#order-details-button_'+ $index ) );
    console.log()
    if(orderButton.hasClass('active'))
      orderButton.removeClass('active');
     else
        orderButton.addClass('active');
};


}]);
