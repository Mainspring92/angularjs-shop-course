'use strict';

var siteCtrls = angular.module('siteCtrls', [])

//============ PRODUCTS CONTROLLERS=====================

siteCtrls.controller('siteProducts',['$scope', '$filter','cartSrv','$http', function($scope, $filter, cartSrv, $http){

  $http({
    method: 'GET',
    url: 'model/produkty.json'
  }).then(function successCallback( response ){
    $scope.products = response.data;
  },function errorCallback( response ){
    console.log('Blad pobrania pliku');
  });

  $scope.addToCart = function( product ){

      cartSrv.add( product );
  };

}]);

siteCtrls.controller('showProduct',['$routeParams','$scope','$http','cartSrv', function($routeParams, $scope, $http, cartSrv){
  $http({
    method: 'GET',
    url: 'model/produkty.json'
  }).then(function successCallback( response ){
    $scope.products = response.data;
    $scope.product = $scope.products[$routeParams.id];
  },function errorCallback( response ){
    console.log('Blad pobrania pliku');
  });

  $scope.addToCart = function( product ){

      cartSrv.add( product );
  };

}]);

//============ ORDERS CONTROLLERS=====================

siteCtrls.controller('siteOrders',['$scope', '$http', function($scope, $http){

$http({
  method: 'GET',
  url: 'model/orders.json'
}).then(function successCallback( response ){
  $scope.orders = response.data;
},function errorCallback( response ){
  console.log('Blad pobrania pliku');
});

}]);

//============ EMPTY CART =====================

siteCtrls.controller('cartCtrl',['$scope', '$http','cartSrv', function($scope, $http, cartSrv){

$scope.cart = cartSrv.show();

$scope.emptyCard = function(){
  cartSrv.empty();
  };

  $scope.total = function(){
    var total = 0;
    angular.forEach($scope.cart, function(item){
        total+= item.qty*item.price;
    });
    return total;
  };

  $scope.removeItem= function($index){

    $scope.cart.splice($index, 1)
    cartSrv.update($scope.cart);

  };

  $scope.setOrder = function ( $event ) {

		// TODO: sprawdź czy użytkownik jest zalogowany

		var loggedIn = true;
		if ( !loggedIn )
		{
			$scope.alert = { type : 'warning' , msg : 'Musisz być zalogowany, żeby złożyć zamówienie.' };
			$event.preventDefault();
			return false;
		}

		// TODO: zapisz zamówienie w bazie

		console.log( $scope.total() );
		console.log( $scope.cart );

		$scope.alert = { type : 'success' , msg : 'Zamówienie złożone. Nie odświeżaj strony. Trwa przekierowywanie do płatności...' };
		cartSrv.empty();

		$event.preventDefault();
		$( '#paypalForm' ).submit();
	};

  $scope.$watch(function(){
    cartSrv.update($scope.cart);
  });

}]);

siteCtrls.controller('orders',['$scope', '$http', function($scope, $http){

$http({
  method: 'GET',
  url: 'model/orders.json'
}).then(function successCallback( response ){
  $scope.orders = response.data;
},function errorCallback( response ){
  console.log('Blad pobrania pliku');
});
}]);

// LOGIN & REGISTER

siteCtrls.controller('login',['$scope', '$http', function($scope, $http){

$scope.input = {};

// TODO: pobrac dane i przeslac do bazy
$scope.formSubmit = function(){
  $scope.errors = {};
  $scope.errors.login = 'Bledne haslo lub email';
};
}]);

siteCtrls.controller('register',['$scope', '$http', function($scope, $http){
$scope.input = {};

$scope.formSubmit = function(){
  $scope.errors = {};
  $scope.errors.name = 'przykladowy blad';
  $scope.submit = true;
  console.log($scope.input);
};


// TODO: pobrac dane i przeslac do bazy

}]);
