'use strict';

var controllersNav = angular.module('controllersNav', [])



controllersNav.controller('navigation',['$scope', 'cartSrv', '$location', function($scope, cartSrv, $location){

//============ SET NAV=====================
$scope.navigation = function(){
  if (/^\/admin/.test($location.path()))
    return 'partials/admin/navigation.html';
  else
    return 'partials/site/navigation.html'
};


  $scope.isActive = function( path ){
    return $location.path() === path;
    console.log(path);
  };

  $scope.$watch(function(){
    $scope.cartDisplay = cartSrv.show().length;
  // console.log($scope.cartDisplay);
  });
}]);
