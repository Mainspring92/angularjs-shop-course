'use district';

var app = angular.module('app',['ngRoute', 'angular-storage','controllersNav','siteCtrls','adminCtrls','myServices']);

app.config( ['$routeProvider','$httpProvider' , function($routeProvider,$httpProvider){

  $routeProvider.when( '/jquery', {
    templateUrl : 'jquery.html'

  });

  $routeProvider.when( '/', {
    redirectTo : 'home/'

  });

  //===============CART==================
  $routeProvider.when( '/cart', {
    controller : 'cartCtrl',
    templateUrl : 'partials/site/cart.html'

  });

//===============PRODUCTS SITE==================
$routeProvider.when( '/products', {
  controller : 'siteProducts',
  templateUrl : 'partials/site/products.html'

});

$routeProvider.when('/product/:id', {
    controller:'showProduct',
    templateUrl:'partials/site/product-show.html'
});

//===============SITE ORDERS==================
  $routeProvider.when('/orders', {
    controller:'siteOrders',
    templateUrl:'partials/site/orders.html'
  });

//===============PRODUCTS==================
  $routeProvider.when( '/admin/products', {
    controller : 'products',
    templateUrl : 'partials/admin/products.html'

  });

  $routeProvider.when('/admin/product/edit/:id', {
      controller:'productEdit',
      templateUrl:'partials/admin/product-edit.html'
  });


  $routeProvider.when('/admin/product/create/', {
      controller:'productCreate',
      templateUrl:'partials/admin/product-create.html'
  });

//===============USERS==================
  $routeProvider.when('/admin/users', {
    controller:'users',
    templateUrl:'partials/admin/users.html'
  });

  $routeProvider.when('/admin/user/edit/:id', {
      controller:'userEdit',
      templateUrl:'partials/admin/user-edit.html'
  });

  $routeProvider.when('/admin/user/create/', {
    controller:'userCreate',
    templateUrl:'partials/admin/new-user.html'
  });

  //===============ORDERS==================
    $routeProvider.when('/admin/orders', {
      controller:'orders',
      templateUrl:'partials/admin/orders.html'
    });
    //===============LOGIN & REGISTER==================
      $routeProvider.when('/login', {
        controller:'login',
        templateUrl:'partials/site/login.html'
      });

      $routeProvider.when('/register', {
        controller:'register',
        templateUrl:'partials/site/register.html'
      });

  // $routeProvider.otherwise({
  //   redirectTo: '/404',
  //   templateUrl: 'partials/404.html'
  // });

}] );
